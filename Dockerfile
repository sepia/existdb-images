# syntax=docker/dockerfile:1
ARG exist_version

ARG download_dir=/dwnld
ARG exist_parent_image=gcr.io/distroless/java
ARG exist_parent_image_tag=8


FROM debian:11-slim as builder
# hadolint ignore=DL3008
RUN apt-get update \
  && apt-get install -y --no-install-recommends \
  expat \
  fontconfig \
  liblcms2-2 \
  fonts-dejavu-core \
  wget

ARG download_dir
WORKDIR ${download_dir}
RUN wget \
      --no-verbose \
      --tries=3 \
      --no-check-certificate \
      https://github.com/eXist-db/atom-editor-support/releases/download/v1.1.0/atom-editor-1.1.0.xar


FROM ${exist_parent_image}:${exist_parent_image_tag} AS exist-prod
ARG build_date
ARG exist_version
ARG vcs_ref

ENV EXIST_HOME=/exist
ENV CLASSPATH=/exist/lib/exist.uber.jar

ENV JAVA_TOOL_OPTIONS="\
  -Dfile.encoding=UTF8 \
  -Dsun.jnu.encoding=UTF-8 \
  -Djava.awt.headless=true \
  -Dorg.exist.db-connection.cacheSize=256M \
  -Dorg.exist.db-connection.pool.max=20 \
  -Dlog4j.configurationFile=/exist/etc/log4j2.xml \
  -Dexist.home=/exist \
  -Dexist.configurationFile=/exist/etc/conf.xml \
  -Djetty.home=/exist \
  -Dexist.jetty.config=/exist/etc/jetty/standard.enabled-jetty-configs \
  -XX:+UnlockExperimentalVMOptions \
  -XX:+UseCGroupMemoryLimitForHeap \
  -XX:+UseG1GC \
  -XX:+UseStringDeduplication \
  -XX:MaxRAMFraction=1 \
  -XX:+ExitOnOutOfMemoryError"

# TODO: set ALL labels
LABEL org.label-schema.build-date="${build_date}"
LABEL org.label-schema.schema-version="1.0"
LABEL org.label-schema.url="https://gitlab.gwdg.de/sepia/existdb-images"
LABEL org.label-schema.vcs-ref="${vcs_ref}"
LABEL org.label-schema.vcs-url="https://gitlab.gwdg.de/sepia/existdb-images.git"
LABEL org.label-schema.vendor="sepia"
LABEL org.label-schema.version="${exist_version}+${vcs_ref}${debug_flag}"

# Dependencies of Apache FOP
COPY --from=builder /usr/lib/x86_64-linux-gnu/libfreetype.so.6 /usr/lib/x86_64-linux-gnu/libfreetype.so.6
COPY --from=builder /usr/lib/x86_64-linux-gnu/liblcms2.so.2 /usr/lib/x86_64-linux-gnu/liblcms2.so.2
COPY --from=builder /usr/lib/x86_64-linux-gnu/libpng16.so.16 /usr/lib/x86_64-linux-gnu/libpng16.so.16
COPY --from=builder /usr/lib/x86_64-linux-gnu/libfontconfig.so.1 /usr/lib/x86_64-linux-gnu/libfontconfig.so.1
COPY --from=builder /etc/fonts /etc/fonts
COPY --from=builder /lib/x86_64-linux-gnu/libexpat.so.1 /lib/x86_64-linux-gnu/libexpat.so.1
COPY --from=builder /usr/share/fontconfig /usr/share/fontconfig
COPY --from=builder /usr/share/fonts/truetype/dejavu /usr/share/fonts/truetype/dejavu

# eXist-db
ARG artifacts_dir
COPY $artifacts_dir/LICENSE /exist/LICENSE
COPY $artifacts_dir/autodeploy /exist/autodeploy
COPY $artifacts_dir/etc /exist/etc
COPY $artifacts_dir/lib /exist/lib
COPY $artifacts_dir/logs /exist/logs

# Apply production logging configuration
COPY conf/prod-logging.xml /exist/etc/log4j2.xml

# Overwrite default entrypoint and cmd to allow execution of arbitrary commands
ENTRYPOINT ["java"]
CMD ["org.exist.start.Main", "jetty"]

COPY Dockerfile /Dockerfile




FROM exist-prod as exist-debug

LABEL org.label-schema.description="This is a development image for exist-db. It adds a custom logging configuration and a supporting package for syncing with VSCode."

# Add the support package
ARG download_dir
COPY --from=builder ${download_dir} /exist/autodeploy/

# Apply debug logging configuration
COPY conf/debug-logging.xml /exist/etc/log4j2.xml
