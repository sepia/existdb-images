# Applied updates, patches, and default configuration

## 5.3.1

[Audit the changes between the release and the updated branch.](https://gitlab.gwdg.de/sepia/existdb/-/compare/eab57f3f3b4cdd604fa7a9da0e6cec59ab66638c...5.3.1-updates?from_project_id=20809)

package name|original version|updated version|closed vulnerabilities
------------|----------------|---------------|----------------------
commons-compress|1.20        |1.21           | CVE-2021-35515, CVE-2021-35516, CVE-2021-35517, CVE-2021-36090
commons-beanutils| 1.8.2     | 1.9.4         | CVE-2014-0114, CVE-2019-10086
jetty       |9.4.42.v20210604|9.4.43.v20210629| CVE-2021-34429
jsoup       | 1.13.1         | 1.14.3        | CVE-2021-37714
log4j       | 2.15.0         | 2.17.1        | CVE-2021-45046, CVE-2021-45105, CVE-2021-44832
protobuf-java| 3.15.6        | 3.19.3        | CVE-2021-22569
pdfbox      | 2.0.23         | 2.0.25        | CVE-2021-31811, CVE-2021-31812

- remove docker image push
- rename debian package `ttf-dejavu-core` to `fonts-dejavu-core` (was transitive, removed in bullseye)

## 5.2.0

[Audit the changes between the release and the updated branch.](https://gitlab.gwdg.de/sepia/existdb/-/compare/eXist-5.2.0...5.2.0-updates?from_project_id=20809)

package name|original version|updated version|closed vulnerabilities
------------|----------------|---------------|----------------------
ant         | 1.10.7         | 1.10.12       | CVE-2020-1945, CVE-2020-11979
batik-util  | 1.12           | 1.14          | CVE-2020-11987,
bcprov-jdk15on| 1.64         | 1.69          | CVE-2020-15522
commons-beanutils| 1.8.2     | 1.9.4         | CVE-2014-0114, CVE-2019-10086
commons-compress| 1.18       | 1.21          | CVE-2019-12402, CVE-2021-35515, CVE-2021-35516, CVE-2021-35517, CVE-2021-36090
commons-configuration2 | 2.6 | 2.7           | CVE-2020-1953
commons-io  | 2.6            | 2.7           | CVE-2021-29425
apache.httpcomponents|4.5.11 | 4.5.13        |
jetty       |9.4.26.v20200117|9.4.44.v20210927| CVE-2020-27216, CVE-2020-27218, CVE-2020-27223, CVE-2021-28165, CVE-2021-34428
jsoup       | 1.13.1         | 1.14.3        | CVE-2021-37714
junit       | 4.13           | 4.13.1        | CVE-2020-15250
log4j       | 2.13.0         | 2.17.1        | CVE-2020-9488, CVE-2021-44228, CVE-2021-45046, CVE-2021-45105, CVE-2021-44832
protobuf-java| 3.15.6        | 3.19.3        | CVE-2021-22569
tika        | 1.23           | 1.27          | CVE-2020-1950, CVE-2020-1951, CVE-2021-28657
xmlgraphics-commons| 2.4     | 2.6           |

- remove docker image push
- disable module `http://exist-db.org/xquery/contentextraction`; update of the tika module < v2 is not enough to remediate security issues; update to >= v2 introduces breaking changes
- rename debian package `ttf-dejavu-core` to `fonts-dejavu-core` (was transitive, removed in bullseye)
- add maven repository "maven-dariah-public" as a surrogate for the packages that are loaded from the no longer available "osgeo"
