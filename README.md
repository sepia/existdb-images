# existdb-images

## Objectives

1. Provide debugging capabilities for development.

    Exist-db does not build a `-debug` version of its image as it is done with distroless base images. This repo builds container images to be used in production and the respective development images tagged with a `-debug` appendix.

1. Unify logging output in production.

    As the logging configuration could be a matter of discussion, two proposals reside in this repository (see `conf/`) and are built into the respective container images.

1. Follow several best practices regarding `Dockerfile`s, container image builds and container image security.
    - [Best practices for writing Dockerfiles](https://docs.docker.com/develop/develop-images/dockerfile_best-practices/)
    - [Best practices for scanning images](https://docs.docker.com/develop/scan-images/)
    - [Intro Guide to Dockerfile Best Practices](https://www.docker.com/blog/intro-guide-to-dockerfile-best-practices/)
    - [Best practices for building containers](https://cloud.google.com/architecture/best-practices-for-building-containers)
    - [10 Docker Security Best Practices](https://snyk.io/blog/10-docker-image-security-best-practices/)

1. Support all [MINOR](https://semver.org/#semantic-versioning-200) versions used in SUB/FE. Currently, these are
    - 5.2.0
    - 5.3.0

1. Apply updates concerning major security issues and [document them](UPDATES.md).

## Usage

In a `Dockerfile`:

```Dockerfile
FROM docker.gitlab.gwdg.de/sepia/existdb-images/existdb:5.3.0-5aad56d0
```

In development, always use the debugging image with the same version/hash combination as you are going to use in production to prevent unexpected outcome. If you would use the above image in production, use the following for development:

```Dockerfile
FROM docker.gitlab.gwdg.de/sepia/existdb-images/existdb:5.3.0-5aad56d0-debug
```

## Development

1. Install prerequisites.
    - Node.js 14 & npm 7
    - Docker CLI 20

1. Install dependencies.

    ```sh
    npm ci
    ```

1. Code.

1. Commit (`git commit`).

## Remarks considering code quality

This repo is [![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg)](https://commitizen.github.io/cz-cli/) and uses [husky🐶](https://www.npmjs.com/package/husky) to configure pre-commit `Dockerfile` linting with [hadolint](https://github.com/hadolint/hadolint) and `YAML` linting with [yamllint](https://github.com/adrienverge/yamllint).
